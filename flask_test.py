from flask import Flask, render_template, request, jsonify
import json

app = Flask(__name__)
print(os.getcwd())

def get_json_data():
    # просто получаем статичные тестовые данные
    with open('data.json', mode='r', encoding='utf-8') as jsonfl:
        data = json.load(jsonfl)
        return data


def send_email(email, text_msg):
    # это метод-заглушка. Здесь должен быть твой код для отправки сообщения
    pass

@app.route('/', methods=['GET'])
def profile():
    # этот метод возвращает только первоначальную страницу
    if request.method == 'GET':
        data = [{'id': rw['id'], 'description': rw['description']} for rw in get_json_data()]
        return render_template(
            'profile.html', 
            data = data,
        )


@app.route('/api/get_data', methods=['POST'])
def get_data():
    # этот метод возвращает только данные конкретной записи
    incom_id = request.values['id']
    res = None
    for rw in get_json_data():
        if rw['id'] == incom_id:
            res = rw
            return jsonify({
                'id': incom_id,
                'born': res['born_date'],
                'number': res['phone_number'],
            })


@app.route('/api/send_msg', methods=['POST'])
def send_msg():
    # этот метод отправляет сообщение и возвращает результат операции
    incom_id = request.values['id']
    incom_txt = request.values['txt']
    for rw in get_json_data():
        if rw['id'] == incom_id:
            send_email(
                email = rw['email'],
                text_msg= incom_txt,
            )
            return jsonify({
                'txt': "Сообщение отправлено успешно",
            })
    return jsonify({
        'txt': "Ошибка отправки сообщения",
    })
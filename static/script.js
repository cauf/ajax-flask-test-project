$("document").ready(function () {

    // ajax settings
    $.ajaxSetup({
        method: 'POST',
        dataType: "json"
    });


    // ajax requests
    function getData(askId) {
        $.ajax({
            url: '/api/get_data',
            data: {
                id: askId
            },
            success: function (incomData) {
                $('#added').show();
                $('#born').text(incomData.born);
                $('#number').text(incomData.number);
                $('#target-id').text(incomData.id);
            }
        })
    };

    function sendMsg(targetId, textMsg) {
        $.ajax({
            url: '/api/send_msg',
            data: {
                id: targetId,
                txt: textMsg
            },
            success: function (incomData) {
                $('#msg').show().text(incomData.txt);
            }
        })
    }


    // onload
    $('#added').hide();
    $('#msg').hide();
    $('#target-id').hide();


    // onclick
    $("#process").click(function () {
        var sel_id = $("#sel").val()[0];
        getData(sel_id);
    });

    $("#send").click(function () {
        var txt = $('#text-msg').val();
        var sel_id = $('#target-id').text();
        sendMsg(sel_id, txt);
    })
})
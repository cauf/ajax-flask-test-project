# Развертывание

```
> python -m venv env
> env/Scripts/activate.bat
(env) > pip install -r requirements.pip
```

# Запуск

```
(env) > ./run.bat
```
